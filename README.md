[![Quality Gate Status](https://sonar.com/api/project_badges/measure?project=api-better-together&metric=alert_status)](https://sonar.com/dashboard?id=api-better-together)

# BetterTogether API

> API responsible for offering customers exclusive products on the digital platform

![image](https://bammgraphix.com/wp-content/uploads/2019/11/wq-love-011_blk.png)

## Requirements
```sh
Java 11
Plugin Lombok
```

## Documentation

* [Swagger](http://swagger.com:8080/api/bettertogether/swagger-ui/index.html?configUrl=/api/bettertogether/v3/api-docs/swagger-config)
* [Wiki](https://confluence.com.br/pages/viewpage.action?spaceKey=SPE&title=Better+Together)

## Install in Linux:

**Java 11 - SDKMAN:**

* [SdkMan](https://sdkman.io/install)

**Lombok plugin:**

* [Intellij](https://projectlombok.org/setup/intellij)
* [Eclipse](https://projectlombok.org/setup/eclipse)

## How to test the API

**Run the command:**

```sh
$ mvn test
```

## How to package the API 

**Package:**

```sh
$ mvn clean package
```

## Note for when to run on Intellij

**Set the environment variable:**

```sh 
SPRING_CONFIG_LOCATION=classpath:/config/local/application.yml
```

## How to run the API with Docker?

**Docker:**

* [Docker install](https://docs.docker.com/get-docker/)

**Docker compose:**

* [Docker Compose](https://docs.docker.com/compose)

**Create image API**
 
* The ${VERSION} value is equal to the version value in pom.xml
* It's necessary the values of the environment variable in src/main/resources/config/local/application.yml are filled

```sh 
$ docker build -f .docker/local/Dockerfile.api --build-arg VERSION=${VERSION} -t api-better-together .
```

**Create containers**

```sh 
$ cd .docker/local
$ docker-compose up -d
```
