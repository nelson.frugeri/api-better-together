**What type of change does this PR introduce? (Bug fix, feature, docs update, tests enhancement, ...)**
e.g. Feature.

**What is the current behavior? (You can connect to an issue or a ticket here)**
e.g. The application does not yet have a partner register.

**What is the new behavior?**
e.g. CRUD implemented by partners.

**Does this PR introduce any break changes?**
Shouldn't =)

**Please make sure the PR meets all requirements (completed by the reviewers):**

- [] Meets the premises of [CONTRIBUTING]

**Other information (Observations, prints, or any other type of information you want to post):**