package com.api.bettertogether.service.reservation;

import com.api.bettertogether.api.v1.dto.response.ResponseBodyReservationDto;
import com.api.bettertogether.exception.InternalServerErrorException;

import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ReservationService {

  private static final String ENDPOINT_PATH = "/selfmanagement/reservations/reservations";

  private static final String BRAND = "BRAND";

  private static final String CHANNEL = "MOBILE";

  private static final String SITE = "BRA";

  private static final String LANGUAGE = "pt";

  @Value("${service.host}")
  private String host;

  @Autowired
  private RestTemplate restTemplate;

  public ResponseEntity<ResponseBodyReservationDto> findByAccountId(final String accountId) {
    try {
      return restTemplate.getForEntity(
          URI.create(UriComponentsBuilder.fromHttpUrl(host + ENDPOINT_PATH)
              .queryParam("user_id", accountId).queryParam("site", SITE)
              .queryParam("channel", CHANNEL).queryParam("brand", BRAND)
              .queryParam("language", LANGUAGE).queryParam("company", BRAND).toUriString()),
          ResponseBodyReservationDto.class);
    } catch (RestClientException ex) {
      throw new InternalServerErrorException(ex);
    }
  }
}
