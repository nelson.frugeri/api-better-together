package com.api.bettertogether.business;

import com.api.bettertogether.domain.SellerDomain;

public interface SellerBusiness {

  SellerDomain save(SellerDomain sellerDomain);
}
