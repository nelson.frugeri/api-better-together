package com.api.bettertogether.business;

import com.api.bettertogether.domain.ImageDomain;
import com.api.bettertogether.domain.ProductDomain;

import java.util.List;

public interface ProductBusiness {

  ProductDomain findById(Long id, String accountId);

  List<ProductDomain> findAll(Boolean enabled, String accountId);

  ProductDomain create(ProductDomain productDomain);

  List<ImageDomain> createImages(Long id, List<ImageDomain> imageDomainList);
}
