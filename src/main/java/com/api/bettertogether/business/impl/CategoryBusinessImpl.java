package com.api.bettertogether.business.impl;

import com.api.bettertogether.domain.CategoryDomain;
import com.api.bettertogether.repository.CategoryRepository;
import com.api.bettertogether.business.CategoryBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryBusinessImpl implements CategoryBusiness {

  @Autowired
  private CategoryRepository categoryRepository;

  @Override
  public CategoryDomain save(final CategoryDomain categoryDomain) {
    return categoryRepository.save(categoryDomain);
  }
}
