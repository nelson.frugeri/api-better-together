package com.api.bettertogether.business.impl;

import com.api.bettertogether.domain.SellerDomain;
import com.api.bettertogether.repository.SellerRepository;
import com.api.bettertogether.business.SellerBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SellerBusinessImpl implements SellerBusiness {

  @Autowired
  private SellerRepository sellerRepository;

  @Override
  public SellerDomain save(final SellerDomain sellerDomain) {
    return sellerRepository.save(sellerDomain);
  }
}
