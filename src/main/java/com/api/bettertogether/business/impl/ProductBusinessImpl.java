package com.api.bettertogether.business.impl;

import static java.text.MessageFormat.format;

import com.api.bettertogether.business.ProductBusiness;
import com.api.bettertogether.domain.ImageDomain;
import com.api.bettertogether.domain.ProductDomain;
import com.api.bettertogether.driver.storage.adapter.AwsS3Adapter;
import com.api.bettertogether.exception.NotFoundException;
import com.api.bettertogether.repository.ImageRepository;
import com.api.bettertogether.repository.ProductRepository;
import com.api.bettertogether.service.reservation.ReservationService;
import com.api.bettertogether.api.v1.dto.response.ResponseBodyReservationDto;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductBusinessImpl implements ProductBusiness {

  private static final String CANCELLED = "CANCELLED";

  @Autowired
  private ReservationService reservationService;

  @Autowired
  private AwsS3Adapter awsS3Adapter;

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private ImageRepository imageRepository;

  @Override
  public ProductDomain findById(final Long id, final String accountId) {
    validReservation(reservationService.findByAccountId(accountId).getBody());
    return productRepository.findById(id).orElseThrow(InternalError::new);
  }

  @Override
  public List<ProductDomain> findAll(final Boolean enabled, final String accountId) {
    validReservation(reservationService.findByAccountId(accountId).getBody());
    return productRepository.findByEnabled(enabled, ZonedDateTime.now());
  }

  private void validReservation(final ResponseBodyReservationDto reservations) {
    Optional.ofNullable(reservations).map(ResponseBodyReservationDto::getContent).stream()
        .flatMap(Collection::stream).filter(content -> !content.getStatus().equals(CANCELLED))
        .map(ResponseBodyReservationDto.Content::getFinish)
        .map(ResponseBodyReservationDto.Content.DateDefinition::getDateTime)
        .filter(dateTime -> dateTime.compareTo(LocalDate.now()) >= 0).findFirst()
        .orElseThrow(() -> new NotFoundException("Reservation"));
  }

  @Transactional
  @Override
  public ProductDomain create(final ProductDomain productDomain) {
    productDomain.getImages()
        .forEach(image -> image
            .setExternalLink(createImage(image.getExternalLink(), productDomain.getSeller().getId(),
                productDomain.getCategory().getId(), productDomain.getSku())));

    return productRepository.save(productDomain);
  }

  private String createImage(final String content, final Long sellerId, final Long categoryId,
      final String sku) {

    return awsS3Adapter.create(content, "image/png",
        format("seller-{0}/category-{1}", sellerId, categoryId), sku, "jpg");
  }

  @Transactional
  @Override
  public List<ImageDomain> createImages(final Long id, final List<ImageDomain> imageDomainList) {
    ProductDomain productDomain =
        productRepository.findById(id).orElseThrow(() -> new NotFoundException("Product"));

    imageDomainList.forEach(image -> {
      image.setExternalLink(createImage(image.getExternalLink(), productDomain.getSeller().getId(),
          productDomain.getCategory().getId(), productDomain.getSku()));
      image.setProduct(productDomain);
    });

    return imageRepository.saveAll(imageDomainList);
  }
}
