package com.api.bettertogether.business;

import com.api.bettertogether.domain.CategoryDomain;

public interface CategoryBusiness {
  CategoryDomain save(CategoryDomain categoryDomain);
}
