package com.api.bettertogether.repository;

import com.api.bettertogether.domain.SellerDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerRepository extends JpaRepository<SellerDomain, Long> {
}
