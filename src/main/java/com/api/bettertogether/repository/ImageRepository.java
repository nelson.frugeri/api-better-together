package com.api.bettertogether.repository;

import com.api.bettertogether.domain.ImageDomain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<ImageDomain, Long> {
}
