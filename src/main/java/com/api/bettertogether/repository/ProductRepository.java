package com.api.bettertogether.repository;

import com.api.bettertogether.domain.ProductDomain;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductDomain, Long> {

  @Query(value = "SELECT product FROM ProductDomain product " + "WHERE product.enabled = :enabled "
      + "AND product.promoExpiry >= :promoExpiry")
  List<ProductDomain> findByEnabled(@Param("enabled") Boolean enabled,
      @Param("promoExpiry") ZonedDateTime promoExpiry);
}
