package com.api.bettertogether.repository;

import com.api.bettertogether.domain.CategoryDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryDomain, Long> {
}
