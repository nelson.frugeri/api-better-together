package com.api.bettertogether.driver.storage.port;

import lombok.NonNull;

public interface StoragePort {

  String create(@NonNull String content, @NonNull String type, @NonNull String folder,
      @NonNull String name, @NonNull String extension);

}
