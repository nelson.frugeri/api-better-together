package com.api.bettertogether.driver.storage.adapter;

import static java.text.MessageFormat.format;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.api.bettertogether.driver.storage.port.StoragePort;
import com.api.bettertogether.exception.InternalServerErrorException;
import com.api.bettertogether.driver.aws.component.AwsConfig;

import java.io.ByteArrayInputStream;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AwsS3Adapter implements StoragePort {

  @Value("${application.env}")
  private String env;

  @Value("${aws.s3.host}")
  private String host;

  @Value("${aws.s3.bucketName}")
  private String bucketName;

  @Value("${aws.s3.cacheControl}")
  private String cacheControl;

  @Autowired
  private AwsConfig awsConfig;

  @Override
  public String create(final String content, final String type, final String folder,
      final String name, final String extension) {

    final var path =
        format("{0}/{1}/{2}-{3}.{4}", env, folder, name, UUID.randomUUID().toString(), extension);

    final var imageByte = Optional
        .ofNullable(org.apache.commons.codec.binary.Base64.decodeBase64((content).getBytes()))
        .orElseThrow(InternalServerErrorException::new);

    Optional
        .ofNullable(awsConfig.amazonS3().putObject(bucketName, path,
            new ByteArrayInputStream(imageByte), getObjectMetadata(imageByte, type)))
        .orElseThrow(InternalServerErrorException::new);

    return format("{0}/{1}", host, path);
  }

  private ObjectMetadata getObjectMetadata(final byte[] imageByte, final String type) {

    ObjectMetadata metadata = new ObjectMetadata();
    metadata.setContentLength(imageByte.length);
    metadata.setContentType(type);
    metadata.setCacheControl(cacheControl);
    return metadata;
  }
}
