package com.api.bettertogether.driver.aws.component;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.api.bettertogether.driver.aws.credential.AwsCredential;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsConfig {

  @Value("${aws.regionName}")
  private String regionName;

  @Autowired
  private AwsCredential awsCredential;

  @Bean
  public AmazonS3 amazonS3() {
    return AmazonS3ClientBuilder.standard().withCredentials(awsCredential.provider())
        .withRegion(regionName).build();
  }
}
