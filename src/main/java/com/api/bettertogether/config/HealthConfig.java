package com.api.bettertogether.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;

public class HealthConfig implements HealthIndicator {

  @Override
  public Health health() {
    return Health.up().build();
  }
}
