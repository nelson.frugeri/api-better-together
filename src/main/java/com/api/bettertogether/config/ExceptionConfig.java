package com.api.bettertogether.config;

import static java.text.MessageFormat.format;

import com.api.bettertogether.exception.InternalServerErrorException;
import com.api.bettertogether.exception.NotFoundException;
import com.api.bettertogether.api.v1.dto.common.ErrorMessageDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionConfig {

  @ExceptionHandler(InternalServerErrorException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public ErrorMessageDto exception(final InternalServerErrorException ex) {
    return ErrorMessageDto.builder().code("ERR-0001").type("Internal server error.")
        .message("Was encountered an error when processing your request.").build();
  }

  @ExceptionHandler(NotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public ResponseEntity<ErrorMessageDto> exception(final NotFoundException ex) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(ErrorMessageDto.builder().code("ERR-0002").type("Not found.")
            .message(format("{0} not found.", ex.getParameters())).build());
  }

  @ExceptionHandler(MissingRequestHeaderException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorMessageDto exception(final MissingRequestHeaderException ex) {
    return ErrorMessageDto.builder().code("ERR-0003").type("Bad request.")
        .message(format("Missing request header {0}.", ex.getHeaderName())).build();
  }
}
