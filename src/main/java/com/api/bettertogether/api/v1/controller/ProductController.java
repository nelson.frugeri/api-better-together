package com.api.bettertogether.api.v1.controller;

import com.api.bettertogether.api.v1.dto.common.ImageCreateDto;
import com.api.bettertogether.api.v1.dto.common.ImageDto;
import com.api.bettertogether.api.v1.dto.common.ProductDto;
import com.api.bettertogether.api.v1.dto.request.RequestBodyCreateProductDto;
import com.api.bettertogether.api.v1.dto.response.ResponseBodyCreateProductDto;
import com.api.bettertogether.api.v1.dto.response.ResponseBodyFindAllProductDto;
import com.api.bettertogether.api.v1.mapper.ProductMapper;
import com.api.bettertogether.business.ProductBusiness;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/better-together/products")
public class ProductController {

  @Autowired
  private ProductBusiness productBusiness;

  @GetMapping(value = "/{id}")
  public ResponseEntity<ProductDto> findById(@PathVariable("id") Long id,
      @RequestHeader(name = "accountId") String accountId) {
    System.out.println("TEST");
    return ResponseEntity.status(HttpStatus.OK)
        .body(ProductMapper.serializeFindById(productBusiness.findById(id, accountId)));
  }

  @GetMapping
  public ResponseEntity<ResponseBodyFindAllProductDto> findAll(
      @RequestParam(name = "enabled") @Valid final Boolean enabled,
      @RequestHeader(name = "accountId") String accountId) {
    return ResponseEntity.status(HttpStatus.OK)
        .body(ProductMapper.serializeList(productBusiness.findAll(enabled, accountId)));
  }

  @ResponseBody
  @PostMapping
  public ResponseEntity<ResponseBodyCreateProductDto> create(
      @RequestBody @Valid final RequestBodyCreateProductDto requestBody) {
    return ResponseEntity.status(HttpStatus.CREATED).body(
        ProductMapper.serialize(productBusiness.create(ProductMapper.deserialize(requestBody))));
  }

  @ResponseBody
  @PostMapping("/{id}/images")
  public ResponseEntity<List<ImageDto>> createImage(@PathVariable("id") final Long id,
      @RequestBody @Valid final List<ImageCreateDto> imageCreateDtos) {
    return ResponseEntity.status(HttpStatus.CREATED).body(ProductMapper.addImages(
        productBusiness.createImages(id, ProductMapper.addImagesDomain(imageCreateDtos))));
  }
}
