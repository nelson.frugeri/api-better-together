package com.api.bettertogether.api.v1.dto.common;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class ImageCreateDto implements Serializable {

  private static final long serialVersionUID = -6614767982442359332L;

  private String content;
  private Boolean isCover;
  private Boolean enabled;
}
