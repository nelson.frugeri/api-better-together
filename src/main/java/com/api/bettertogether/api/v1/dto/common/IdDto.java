package com.api.bettertogether.api.v1.dto.common;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class IdDto implements Serializable {

  private static final long serialVersionUID = -8867699428777903814L;

  private Long id;
}
