package com.api.bettertogether.api.v1.dto.common;

import java.io.Serializable;
import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class PromoDto implements Serializable {

  private static final long serialVersionUID = -3496804215443645906L;

  private String code;
  private ZonedDateTime expiry;
}
