package com.api.bettertogether.api.v1.dto.request;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class RequestBodyCreateSellerDto implements Serializable {

  private static final long serialVersionUID = -7636440316817601396L;

  private String name;
  private String description;
  private String cnpj;
  private Boolean enabled;
}
