package com.api.bettertogether.api.v1.dto.common;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class ImageDto implements Serializable {

  private static final long serialVersionUID = -1692289672847628837L;

  private Long id;
  private String externalLink;
  private Boolean isCover;
  private Boolean enabled;
}
