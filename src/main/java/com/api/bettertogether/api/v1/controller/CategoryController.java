package com.api.bettertogether.api.v1.controller;

import com.api.bettertogether.api.v1.dto.request.RequestBodyCreateCategoryDto;
import com.api.bettertogether.api.v1.mapper.CategoryMapper;
import com.api.bettertogether.v1.dto.request.RequestBodyCreateCategoryDto;
import com.api.bettertogether.v1.dto.response.ResponseBodyCreateCategoryDto;
import com.api.bettertogether.v1.mapper.CategoryMapper;
import com.api.bettertogether.business.CategoryBusiness;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/better-together/categories")
public class CategoryController {

  @Autowired
  private CategoryBusiness categoryBusiness;

  @ResponseBody
  @PostMapping
  public ResponseEntity<ResponseBodyCreateCategoryDto> create(
      @RequestBody @Valid final RequestBodyCreateCategoryDto requestBody) {
    return ResponseEntity.status(HttpStatus.CREATED).body(
        CategoryMapper.serialize(categoryBusiness.save(CategoryMapper.deserialize(requestBody))));
  }
}
