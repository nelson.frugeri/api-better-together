package com.api.bettertogether.api.v1.controller;

import com.api.bettertogether.api.v1.dto.request.RequestBodyCreateSellerDto;
import com.api.bettertogether.v1.dto.request.RequestBodyCreateSellerDto;
import com.api.bettertogether.v1.dto.response.ResponseBodyCreateSellerDto;
import com.api.bettertogether.v1.mapper.SellerMapper;
import com.api.bettertogether.business.SellerBusiness;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/better-together/sellers")
public class SellerController {

  @Autowired
  private SellerBusiness sellerBusiness;

  @ResponseBody
  @PostMapping
  public ResponseEntity<ResponseBodyCreateSellerDto> create(
      @RequestBody @Valid final RequestBodyCreateSellerDto requestBody) {

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(SellerMapper.serialize(sellerBusiness.save(SellerMapper.deserialize(requestBody))));
  }
}
