package com.api.bettertogether.api.v1.mapper;

import com.api.bettertogether.api.v1.dto.request.RequestBodyCreateSellerDto;
import com.api.bettertogether.domain.SellerDomain;
import com.api.bettertogether.api.v1.dto.response.ResponseBodyCreateSellerDto;
import org.springframework.stereotype.Component;

@Component
public class SellerMapper {

  private SellerMapper() {}

  public static SellerDomain deserialize(final RequestBodyCreateSellerDto requestBody) {
    return SellerDomain.builder().name(requestBody.getName())
        .description(requestBody.getDescription()).cnpj(requestBody.getCnpj())
        .enabled(requestBody.getEnabled()).build();
  }

  public static ResponseBodyCreateSellerDto serialize(final SellerDomain seller) {
    return ResponseBodyCreateSellerDto.builder().id(seller.getId()).name(seller.getName())
        .description(seller.getDescription()).cnpj(seller.getCnpj()).enabled(seller.getEnabled())
        .build();
  }
}
