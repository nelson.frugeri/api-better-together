package com.api.bettertogether.api.v1.dto.common;

import java.io.Serializable;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ErrorMessageDto implements Serializable {

  private static final long serialVersionUID = 6347980348820100308L;

  private final String code;
  private final String type;
  private final String message;
}
