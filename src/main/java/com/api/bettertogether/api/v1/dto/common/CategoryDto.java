package com.api.bettertogether.api.v1.dto.common;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class CategoryDto implements Serializable {

  private static final long serialVersionUID = 8174117234448730651L;

  private Long id;
  private String name;
  private String description;
  private Boolean enabled;
}
