package com.api.bettertogether.api.v1.dto.response;

import com.api.bettertogether.api.v1.dto.common.MetaDto;
import com.api.bettertogether.api.v1.dto.common.ProductDto;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class ResponseBodyFindAllProductDto implements Serializable {

  private static final long serialVersionUID = -2737272070290357413L;

  private MetaDto meta;
  private List<ProductDto> products;
}
