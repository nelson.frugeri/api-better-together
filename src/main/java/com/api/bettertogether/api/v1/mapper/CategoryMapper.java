package com.api.bettertogether.api.v1.mapper;

import com.api.bettertogether.api.v1.dto.request.RequestBodyCreateCategoryDto;
import com.api.bettertogether.domain.CategoryDomain;
import com.api.bettertogether.api.v1.dto.response.ResponseBodyCreateCategoryDto;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {

  private CategoryMapper() {}

  public static CategoryDomain deserialize(final RequestBodyCreateCategoryDto requestBody) {

    return CategoryDomain.builder().name(requestBody.getName())
        .description(requestBody.getDescription()).enabled(requestBody.getEnabled()).build();
  }

  public static ResponseBodyCreateCategoryDto serialize(final CategoryDomain category) {

    return ResponseBodyCreateCategoryDto.builder().id(category.getId()).name(category.getName())
        .description(category.getDescription()).enabled(category.getEnabled()).build();
  }
}
