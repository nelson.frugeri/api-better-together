package com.api.bettertogether.api.v1.dto.request;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class RequestBodyCreateCategoryDto implements Serializable {

  private static final long serialVersionUID = 4081229856656466819L;

  private String name;
  private String description;
  private Boolean enabled;
}
