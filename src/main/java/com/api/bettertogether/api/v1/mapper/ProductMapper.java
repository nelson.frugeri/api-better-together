package com.api.bettertogether.api.v1.mapper;

import com.api.bettertogether.api.v1.dto.common.CategoryDto;
import com.api.bettertogether.api.v1.dto.common.IdDto;
import com.api.bettertogether.api.v1.dto.common.ImageCreateDto;
import com.api.bettertogether.api.v1.dto.common.ImageDto;
import com.api.bettertogether.api.v1.dto.common.MetaDto;
import com.api.bettertogether.api.v1.dto.common.ProductDto;
import com.api.bettertogether.api.v1.dto.common.PromoDto;
import com.api.bettertogether.api.v1.dto.common.SellerDto;
import com.api.bettertogether.api.v1.dto.request.RequestBodyCreateProductDto;
import com.api.bettertogether.api.v1.dto.response.ResponseBodyCreateProductDto;
import com.api.bettertogether.domain.CategoryDomain;
import com.api.bettertogether.domain.ImageDomain;
import com.api.bettertogether.domain.ProductDomain;
import com.api.bettertogether.domain.SellerDomain;
import com.api.bettertogether.api.v1.dto.response.ResponseBodyFindAllProductDto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

  private ProductMapper() {}

  public static ProductDto serializeFindById(final ProductDomain product) {

    return ProductDto.builder().id(product.getId())
        .seller(SellerDto.builder().id(product.getSeller().getId())
            .name(product.getSeller().getName()).description(product.getSeller().getDescription())
            .cnpj(product.getSeller().getCnpj()).enabled(product.getSeller().getEnabled()).build())
        .category(CategoryDto.builder().id(product.getCategory().getId())
            .name(product.getCategory().getName())
            .description(product.getCategory().getDescription())
            .enabled(product.getCategory().getEnabled()).build())
        .sku(product.getSku()).name(product.getName()).description(product.getDescription())
        .currency(product.getCurrency()).price(product.getPrice()).discount(product.getDiscount())
        .promo(PromoDto.builder().code(product.getPromoCode()).expiry(product.getPromoExpiry())
            .build())
        .checkoutLink(product.getCheckoutLink()).enabled(product.getEnabled())
        .images(addImageDto(product.getImages())).build();
  }

  public static ResponseBodyFindAllProductDto serializeList(final List<ProductDomain> products) {

    return ResponseBodyFindAllProductDto.builder()
        .meta(MetaDto.builder().total(products.size()).build()).products(addProduct(products))
        .build();
  }

  private static List<ProductDto> addProduct(final List<ProductDomain> products) {

    final var productsDomain = new ArrayList<ProductDto>();

    products.forEach(product -> productsDomain.add(serializeFindById(product)));

    return productsDomain;
  }

  private static List<ImageDto> addImageDto(final List<ImageDomain> images) {

    final var imagesDto = new ArrayList<ImageDto>();

    images.forEach(image -> imagesDto
        .add(ImageDto.builder().id(image.getId()).externalLink(image.getExternalLink())
            .isCover(image.getIsCover()).enabled(image.getEnabled()).build()));

    return imagesDto;
  }

  public static ProductDomain deserialize(final RequestBodyCreateProductDto requestBody) {

    final var product = ProductDomain.builder()
        .seller(SellerDomain.builder().id(requestBody.getSeller().getId()).build())
        .category(CategoryDomain.builder().id(requestBody.getCategory().getId()).build())
        .sku(requestBody.getSku()).name(requestBody.getName())
        .description(requestBody.getDescription()).currency(requestBody.getCurrency())
        .price(requestBody.getPrice()).discount(requestBody.getDiscount())
        .promoCode(requestBody.getPromo().getCode()).promoExpiry(requestBody.getPromo().getExpiry())
        .checkoutLink(requestBody.getCheckoutLink()).enabled(requestBody.getEnabled()).build();

    product.setImages(addImagesDomain(requestBody.getImages(), product));

    return product;
  }

  private static List<ImageDomain> addImagesDomain(final List<ImageCreateDto> images,
      final ProductDomain product) {

    final var imagesDomain = new ArrayList<ImageDomain>();

    images.forEach(image -> imagesDomain
        .add(ImageDomain.builder().product(product).externalLink(image.getContent())
            .isCover(image.getIsCover()).enabled(image.getEnabled()).build()));

    return imagesDomain;
  }

  public static List<ImageDomain> addImagesDomain(final List<ImageCreateDto> images) {

    final var imagesDomain = new ArrayList<ImageDomain>();

    images.forEach(image -> imagesDomain.add(ImageDomain.builder().externalLink(image.getContent())
        .isCover(image.getIsCover()).enabled(image.getEnabled()).build()));

    return imagesDomain;
  }

  public static ResponseBodyCreateProductDto serialize(final ProductDomain product) {

    return ResponseBodyCreateProductDto.builder().id(product.getId())
        .seller(IdDto.builder().id(product.getSeller().getId()).build())
        .category(IdDto.builder().id(product.getCategory().getId()).build()).sku(product.getSku())
        .name(product.getName()).description(product.getDescription())
        .currency(product.getCurrency()).price(product.getPrice()).discount(product.getDiscount())
        .promo(PromoDto.builder().code(product.getPromoCode()).expiry(product.getPromoExpiry())
            .build())
        .checkoutLink(product.getCheckoutLink()).enabled(product.getEnabled())
        .images(addImages(product.getImages())).build();
  }

  public static List<ImageDto> addImages(final List<ImageDomain> imagesDomain) {

    final var images = new ArrayList<ImageDto>();

    imagesDomain.forEach(image -> images
        .add(ImageDto.builder().id(image.getId()).externalLink(image.getExternalLink())
            .isCover(image.getIsCover()).enabled(image.getEnabled()).build()));

    return images;
  }
}
