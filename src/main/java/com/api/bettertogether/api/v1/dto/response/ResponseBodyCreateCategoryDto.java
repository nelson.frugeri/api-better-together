package com.api.bettertogether.api.v1.dto.response;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class ResponseBodyCreateCategoryDto implements Serializable {

  private static final long serialVersionUID = -7387074562541570809L;

  private Long id;
  private String name;
  private String description;
  private Boolean enabled;
}
