package com.api.bettertogether.api.v1.dto.request;

import com.api.bettertogether.api.v1.dto.common.IdDto;
import com.api.bettertogether.api.v1.dto.common.ImageCreateDto;
import com.api.bettertogether.api.v1.dto.common.PromoDto;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class RequestBodyCreateProductDto implements Serializable {

  private static final long serialVersionUID = -5125502160018910508L;

  private IdDto seller;
  private IdDto category;
  private String sku;
  private String name;
  private String description;
  private String currency;
  private BigDecimal price;
  private Integer discount;
  private PromoDto promo;
  private String checkoutLink;
  private Boolean enabled;
  private List<ImageCreateDto> images;
}
