package com.api.bettertogether.api.v1.dto.common;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class ProductDto implements Serializable {

  private static final long serialVersionUID = 4047598921492014107L;

  private Long id;
  private SellerDto seller;
  private CategoryDto category;
  private String sku;
  private String name;
  private String description;
  private String currency;
  private BigDecimal price;
  private Integer discount;
  private PromoDto promo;
  private String checkoutLink;
  private Boolean enabled;
  private List<ImageDto> images;
}
