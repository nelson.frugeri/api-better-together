package com.api.bettertogether.api.v1.dto.response;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class ResponseBodyCreateSellerDto implements Serializable {

  private static final long serialVersionUID = -5070090600742227526L;

  private Long id;
  private String name;
  private String description;
  private String cnpj;
  private Boolean enabled;
}
