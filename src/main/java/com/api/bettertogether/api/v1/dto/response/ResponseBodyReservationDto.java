package com.api.bettertogether.api.v1.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class ResponseBodyReservationDto implements Serializable {

  private static final long serialVersionUID = -7185679709371266291L;

  private List<Content> content;

  @AllArgsConstructor
  @NoArgsConstructor
  @Builder
  @Getter
  public static class Content implements Serializable {

    private static final long serialVersionUID = 8599766379765669125L;

    private String status;
    private DateDefinition finish;

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Getter
    public static class DateDefinition implements Serializable {

      private static final long serialVersionUID = 859976637123669125L;

      @JsonProperty("date_time")
      private LocalDate dateTime;
    }
  }
}
