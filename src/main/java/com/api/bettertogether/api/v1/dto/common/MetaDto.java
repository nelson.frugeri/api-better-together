package com.api.bettertogether.api.v1.dto.common;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class MetaDto implements Serializable {

  private static final long serialVersionUID = 1799486744840347884L;

  private Integer total;
  private Integer limit;
  private Integer offset;
}
