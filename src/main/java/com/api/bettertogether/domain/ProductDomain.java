package com.api.bettertogether.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product")
public class ProductDomain implements Serializable {

  private static final long serialVersionUID = 2909745170879852140L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
  private List<ImageDomain> images;

  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @JoinColumn(name = "seller_id")
  private SellerDomain seller;

  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @JoinColumn(name = "category_id")
  private CategoryDomain category;

  @Column(name = "sku")
  private String sku;

  @Column(name = "name")
  private String name;

  @Column(name = "description")
  private String description;

  @Column(name = "currency")
  private String currency;

  @Column(name = "price")
  private BigDecimal price;

  @Column(name = "discount")
  private Integer discount;

  @Column(name = "promo_code")
  private String promoCode;

  @Column(name = "promo_expiry")
  private ZonedDateTime promoExpiry;

  @Column(name = "checkout_link")
  private String checkoutLink;

  @Column(name = "enabled")
  private Boolean enabled;

  @Column(name = "created_at", updatable = false)
  private ZonedDateTime createdAt;

  @Column(name = "updated_at")
  private ZonedDateTime updatedAt;

  @PrePersist
  protected void prePersist() {
    updatedAt = createdAt = ZonedDateTime.now();
  }

  @PreUpdate
  protected void preUpdate() {
    updatedAt = ZonedDateTime.now();
  }
}
