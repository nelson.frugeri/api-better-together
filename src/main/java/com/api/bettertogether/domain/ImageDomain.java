package com.api.bettertogether.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "image")
public class ImageDomain implements Serializable {

  private static final long serialVersionUID = 5708580852388888541L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "product_id")
  private ProductDomain product;

  @Column(name = "external_link")
  private String externalLink;

  @Column(name = "is_cover")
  private Boolean isCover;

  @Column(name = "enabled")
  private Boolean enabled;

  @Column(name = "created_at", updatable = false)
  private ZonedDateTime createdAt;

  @Column(name = "updated_at")
  private ZonedDateTime updatedAt;

  @PrePersist
  protected void prePersist() {
    updatedAt = createdAt = ZonedDateTime.now();
  }

  @PreUpdate
  protected void preUpdate() {
    updatedAt = ZonedDateTime.now();
  }
}
