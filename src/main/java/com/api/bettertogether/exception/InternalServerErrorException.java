package com.api.bettertogether.exception;

public class InternalServerErrorException extends BaseException {

  private static final long serialVersionUID = -1709686063469968027L;

  public InternalServerErrorException(Object... parameters) {
    super(parameters);
  }
}
