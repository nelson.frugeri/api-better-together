package com.api.bettertogether.exception;

public class NotFoundException extends BaseException {

  private static final long serialVersionUID = 2765525433781346597L;

  public NotFoundException(Object... parameters) {
    super(parameters);
    super.setStatusCode(404);
  }
}
