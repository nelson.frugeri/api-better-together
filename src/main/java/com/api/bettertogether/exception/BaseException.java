package com.api.bettertogether.exception;

import lombok.Getter;
import lombok.Setter;

public class BaseException extends RuntimeException {

  private static final long serialVersionUID = -1365989445082474473L;

  @Getter
  @Setter
  private int statusCode = 500;

  private final transient Object[] parameters;

  BaseException(final Object... parameters) {
    super();
    this.parameters = parameters;
  }

  public Object[] getParameters() {
    return this.parameters.clone();
  }


}
