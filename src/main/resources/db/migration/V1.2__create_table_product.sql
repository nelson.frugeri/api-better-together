CREATE TABLE product (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  seller_id BIGINT NOT NULL,
  category_id BIGINT NOT NULL,
  sku VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255) NOT NULL,
  currency VARCHAR(45),
  price DECIMAL(10,2),
  discount INT,
  promo_code VARCHAR(255),
  promo_expiry TIMESTAMP,
  checkout_link VARCHAR(2083),
  enabled BOOLEAN NOT NULL DEFAULT FALSE,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
  FOREIGN KEY(seller_id) REFERENCES seller(id),
  FOREIGN KEY(category_id) REFERENCES category(id)
);