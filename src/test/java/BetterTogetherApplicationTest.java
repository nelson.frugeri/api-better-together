import com.api.bettertogether.BetterTogetherApplication;
import com.api.bettertogether.template.TemplateTest;
import org.junit.jupiter.api.Test;

public class BetterTogetherApplicationTest extends TemplateTest {

  @Test
  public void main() {
    BetterTogetherApplication
        .main(new String[] {"--spring.config.location=classpath:application-test.yml"});
  }
}
