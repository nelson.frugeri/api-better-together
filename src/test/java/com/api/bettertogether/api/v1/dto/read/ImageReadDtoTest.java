package com.api.bettertogether.api.v1.dto.read;

import com.api.bettertogether.api.v1.dto.common.ImageDto;
import com.api.bettertogether.template.DtoTemplateTest;
import org.junit.Test;

public class ImageReadDtoTest extends DtoTemplateTest<ImageDto> {

  @Test
  public void validateSchema() throws Exception {
    super.validateSchema("/schema/v1/dto/read/image-read.json");
  }
}
