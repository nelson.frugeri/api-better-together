package com.api.bettertogether.api.v1.dto.response;

import com.api.bettertogether.template.DtoTemplateTest;
import org.junit.Test;

public class ResponseBodyReservationDtoTest extends DtoTemplateTest<ResponseBodyReservationDto> {

  @Test
  public void validateSchema() throws Exception {
    super.validateSchema("/schema/v1/dto/response/response-body-reservation.json");
  }
}
