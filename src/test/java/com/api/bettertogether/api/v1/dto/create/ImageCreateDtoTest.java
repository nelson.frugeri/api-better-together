package com.api.bettertogether.api.v1.dto.create;

import com.api.bettertogether.api.v1.dto.common.ImageCreateDto;
import com.api.bettertogether.template.DtoTemplateTest;
import org.junit.Test;

public class ImageCreateDtoTest extends DtoTemplateTest<ImageCreateDto> {

  @Test
  public void validateSchema() throws Exception {
    super.validateSchema("/schema/v1/dto/create/image-create.json");
  }
}
