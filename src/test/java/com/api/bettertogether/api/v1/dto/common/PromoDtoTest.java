package com.api.bettertogether.api.v1.dto.common;

import com.api.bettertogether.template.DtoTemplateTest;
import org.junit.Test;

public class PromoDtoTest extends DtoTemplateTest<PromoDto> {

  @Test
  public void validateSchema() throws Exception {
    super.validateSchema("/schema/v1/dto/common/promo.json");
  }
}
