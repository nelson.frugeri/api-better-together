package com.api.bettertogether.api.v1.controller;

import com.api.bettertogether.api.v1.dto.request.RequestBodyCreateCategoryDto;
import com.api.bettertogether.repository.CategoryRepository;
import com.api.bettertogether.template.TemplateTest;

import java.util.Objects;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class CategoryControllerTest extends TemplateTest {

  @Autowired
  private CategoryRepository categoryRepository;

  @Autowired
  private CategoryController categoryController;

  @Test
  public void createWhenSuccessfully() {
    final var response = categoryController
        .create(RequestBodyCreateCategoryDto.builder().name(this.getFaker().name().fullName())
            .description(this.getFaker().name().title()).enabled(Boolean.TRUE).build());

    final var category =
        categoryRepository.findById(Objects.requireNonNull(response.getBody()).getId())
            .orElseThrow(InternalError::new);

    Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assert.assertEquals(category.getId(), response.getBody().getId());
    Assert.assertEquals(category.getName(), response.getBody().getName());
    Assert.assertEquals(category.getDescription(), response.getBody().getDescription());
    Assert.assertEquals(category.getEnabled(), response.getBody().getEnabled());
  }
}
