package com.api.bettertogether.api.v1.dto.request;

import com.api.bettertogether.template.DtoTemplateTest;
import org.junit.Test;

public class RequestBodyCreateSellerDtoTest extends DtoTemplateTest<RequestBodyCreateSellerDto> {

  @Test
  public void validateSchema() throws Exception {
    super.validateSchema("/schema/v1/dto/request/request-body-create-seller.json");
  }
}
