package com.api.bettertogether.api.v1.controller;

import com.api.bettertogether.api.v1.dto.request.RequestBodyCreateSellerDto;
import com.api.bettertogether.repository.SellerRepository;
import com.api.bettertogether.template.TemplateTest;

import java.util.Objects;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class SellerControllerTest extends TemplateTest {

  @Autowired
  private SellerRepository sellerRepository;

  @Autowired
  private SellerController sellerController;

  @Test
  public void createWhenSuccessfully() {
    final var response = sellerController.create(RequestBodyCreateSellerDto.builder()
        .name(this.getFaker().name().fullName()).description(this.getFaker().name().title())
        .cnpj(this.getFaker().name().title()).enabled(Boolean.TRUE).build());

    final var seller = sellerRepository.findById(Objects.requireNonNull(response.getBody()).getId())
        .orElseThrow(InternalError::new);

    Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assert.assertEquals(seller.getId(), response.getBody().getId());
    Assert.assertEquals(seller.getName(), response.getBody().getName());
    Assert.assertEquals(seller.getDescription(), response.getBody().getDescription());
    Assert.assertEquals(seller.getCnpj(), response.getBody().getCnpj());
    Assert.assertEquals(seller.getEnabled(), response.getBody().getEnabled());
  }
}
