package com.api.bettertogether.api.v1.dto.schema;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.micrometer.core.instrument.util.IOUtils;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ThreadLocalRandom;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Before;

public abstract class ValidateSchemaTest<C extends Serializable> {

  private EnhancedRandomBuilder enhancedRandom;
  private Class<?> clazz;

  @Before
  public void init() {
    clazz = (Class<?>) ((ParameterizedType) this.getClass().getGenericSuperclass())
        .getActualTypeArguments()[0];
    enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
        .seed(ThreadLocalRandom.current().nextLong(100));
  }

  @SneakyThrows
  protected void validateSchema(final String resourceFileName) {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    validateSchema(mapper.writeValueAsString(enhancedRandom.build().nextObject(clazz)),
        resourceFileName);
  }

  @SneakyThrows
  protected void validateSchema(final String json, final String resourceFileName) {
    @Cleanup
    final InputStream in = this.getClass().getResourceAsStream(resourceFileName);
    final String schema = IOUtils.toString(in, StandardCharsets.UTF_8);
    SchemaLoader.builder().schemaJson(new JSONObject(new JSONTokener(schema))).build().load()
        .build().validate(new JSONObject(new JSONTokener(json)));
  }
}
