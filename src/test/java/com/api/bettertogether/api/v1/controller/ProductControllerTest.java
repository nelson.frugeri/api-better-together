package com.api.bettertogether.api.v1.controller;

import static java.text.MessageFormat.format;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.api.bettertogether.api.v1.dto.common.ImageCreateDto;
import com.api.bettertogether.api.v1.dto.common.PromoDto;
import com.api.bettertogether.api.v1.dto.request.RequestBodyCreateProductDto;
import com.api.bettertogether.domain.CategoryDomain;
import com.api.bettertogether.domain.ProductDomain;
import com.api.bettertogether.domain.SellerDomain;
import com.api.bettertogether.driver.aws.component.AwsConfig;
import com.api.bettertogether.exception.InternalServerErrorException;
import com.api.bettertogether.exception.NotFoundException;
import com.api.bettertogether.repository.CategoryRepository;
import com.api.bettertogether.repository.ProductRepository;
import com.api.bettertogether.repository.SellerRepository;
import com.api.bettertogether.api.v1.dto.common.IdDto;
import com.api.bettertogether.api.v1.dto.response.ResponseBodyReservationDto;
import com.api.bettertogether.driver.aws.component.AwsComponent;
import com.api.bettertogether.factory.ProductFactory;
import com.api.bettertogether.template.TemplateTest;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import lombok.NonNull;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class ProductControllerTest extends TemplateTest {

  private static final String RESERVATION = "Reservation";

  private static final String CANCELLED = "CANCELLED";

  private static final String IN_PROGRESS = "IN_PROGRESS";
  private static final ZonedDateTime DATE_DEFAULT =
      ZonedDateTime.of(LocalDate.now(), LocalTime.of(23, 59, 59), ZoneId.systemDefault());

  @Autowired
  AwsComponent awsComponent;

  @Value("${service.host}")
  private String host;

  @MockBean
  private AwsConfig awsConfig;

  @Autowired
  private ProductRepository productrepository;

  @Autowired
  private ProductController productController;

  @Autowired
  private SellerRepository sellerRepository;

  @Autowired
  private CategoryRepository categoryRepository;

  @MockBean
  private RestTemplate restTemplate;

  private SellerDomain seller;

  private CategoryDomain category;

  @BeforeAll
  public void beforeAll() {
    seller = sellerRepository.save(SellerDomain.builder()
        .id(this.getFaker().number().randomNumber()).name(this.getFaker().company().name())
        .description(this.getFaker().company().catchPhrase()).enabled(this.getFaker().bool().bool())
        .build());

    category = categoryRepository.save(CategoryDomain.builder()
        .id(this.getFaker().number().randomNumber()).name(this.getFaker().company().name())
        .description(this.getFaker().company().catchPhrase()).enabled(this.getFaker().bool().bool())
        .build());
  }

  @AfterAll
  public void afterAll() {
    sellerRepository.deleteAll();
    categoryRepository.deleteAll();
  }

  @Test
  public void findByIdWhenSuccessfully() {
    final var accountId = this.getFaker().crypto().md5();

    mockReservationService(accountId, IN_PROGRESS, ResponseBodyReservationDto.Content.DateDefinition
        .builder().dateTime(LocalDate.now()).build());

    productrepository.saveAll(Arrays.asList(
        ProductFactory.productByEnabled(Boolean.FALSE, DATE_DEFAULT, seller, category),
        ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category),
        ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category)));

    Assert.assertEquals(Optional.of(1L), Optional
        .of(Objects.requireNonNull(productController.findById(1L, accountId).getBody()).getId()));
    Assert.assertEquals(Optional.of(2L), Optional
        .of(Objects.requireNonNull(productController.findById(2L, accountId).getBody()).getId()));
    Assert.assertEquals(Optional.of(3L), Optional
        .of(Objects.requireNonNull(productController.findById(3L, accountId).getBody()).getId()));

    productrepository.deleteAll();
  }

  @Test
  public void findByIdWhenNotFinishedReservation() {
    final var accountId = this.getFaker().crypto().md5();

    mockReservationService(accountId, CANCELLED, ResponseBodyReservationDto.Content.DateDefinition
        .builder().dateTime(LocalDate.now()).build());

    productrepository.saveAll(Collections.singletonList(
        ProductFactory.productByEnabled(Boolean.FALSE, DATE_DEFAULT, seller, category)));

    final var exception =
        assertThrows(NotFoundException.class, () -> productController.findById(1L, accountId));

    Assert.assertEquals(RESERVATION, exception.getParameters()[0]);

    productrepository.deleteAll();
  }

  @Test
  public void findAllWhenReservationTimeIsOver() {
    final var accountId = this.getFaker().crypto().md5();

    mockReservationService(accountId, IN_PROGRESS, ResponseBodyReservationDto.Content.DateDefinition
        .builder().dateTime(LocalDate.now().minusDays(10)).build());

    productrepository.saveAll(Collections.singletonList(
        ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category)));

    final var exception = assertThrows(NotFoundException.class,
        () -> productController.findAll(Boolean.TRUE, accountId));

    Assert.assertEquals(RESERVATION, exception.getParameters()[0]);

    productrepository.deleteAll();
  }

  @Test
  public void findAllWhenNotFinishedReservation() {
    final var accountId = this.getFaker().crypto().md5();

    mockReservationService(accountId, CANCELLED, ResponseBodyReservationDto.Content.DateDefinition
        .builder().dateTime(LocalDate.now()).build());

    productrepository.saveAll(Collections.singletonList(
        ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category)));

    final var exception = assertThrows(NotFoundException.class,
        () -> productController.findAll(Boolean.TRUE, accountId));

    Assert.assertEquals(RESERVATION, exception.getParameters()[0]);

    productrepository.deleteAll();
  }

  @Test
  public void findAllProductsWhenPromoExpired() {
    final var accountId = this.getFaker().crypto().md5();

    mockReservationService(accountId, IN_PROGRESS, ResponseBodyReservationDto.Content.DateDefinition
        .builder().dateTime(LocalDate.now()).build());

    productrepository.saveAll(
        Arrays.asList(ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category),
            ProductFactory.productByEnabled(Boolean.TRUE,
                ZonedDateTime.of(1994, 7, 17, 23, 59, 59, 1234, ZoneId.systemDefault()), seller,
                category)));

    Assert.assertEquals(1,
        Objects.requireNonNull(productController.findAll(Boolean.TRUE, accountId).getBody())
            .getProducts().size());

    productrepository.deleteAll();
  }

  @Test
  public void findAllProductsWhenEnabledAndDisabled() {
    final var accountId = this.getFaker().crypto().md5();

    mockReservationService(accountId, IN_PROGRESS, ResponseBodyReservationDto.Content.DateDefinition
        .builder().dateTime(LocalDate.now()).build());

    productrepository.saveAll(Arrays.asList(
        ProductFactory.productByEnabled(Boolean.FALSE, DATE_DEFAULT, seller, category),
        ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category)));

    Assert.assertEquals(1,
        Objects.requireNonNull(productController.findAll(Boolean.TRUE, accountId).getBody())
            .getProducts().size());
    Assert.assertEquals(1,
        Objects.requireNonNull(productController.findAll(Boolean.FALSE, accountId).getBody())
            .getProducts().size());

    productrepository.deleteAll();
  }

  @Test
  public void findAllProductsWhenAllEnabled() {
    final var accountId = this.getFaker().crypto().md5();

    mockReservationService(accountId, IN_PROGRESS, ResponseBodyReservationDto.Content.DateDefinition
        .builder().dateTime(LocalDate.now()).build());

    final var products = productrepository.saveAll(
        Arrays.asList(ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category),
            ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category)));

    final var productsList = productController.findAll(Boolean.TRUE, accountId);

    Assert.assertEquals(products.get(0).getId(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getId());
    Assert.assertEquals(products.get(0).getSeller().getId(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getSeller().getId());
    Assert.assertEquals(products.get(0).getCategory().getId(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getCategory().getId());
    Assert.assertEquals(products.get(0).getSku(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getSku());
    Assert.assertEquals(products.get(0).getName(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getName());
    Assert.assertEquals(products.get(0).getDescription(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getDescription());
    Assert.assertEquals(products.get(0).getPrice(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getPrice());
    Assert.assertEquals(products.get(0).getDiscount(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getDiscount());
    Assert.assertEquals(products.get(0).getPromoCode(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getPromo().getCode());
    Assert.assertEquals(products.get(0).getPromoExpiry(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getPromo().getExpiry());
    Assert.assertEquals(products.get(0).getCheckoutLink(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getCheckoutLink());
    Assert.assertEquals(products.get(0).getEnabled(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getEnabled());
    Assert.assertEquals(products.get(0).getImages().get(0).getId(), Objects
        .requireNonNull(productsList.getBody()).getProducts().get(0).getImages().get(0).getId());
    Assert.assertEquals(products.get(0).getImages().get(0).getExternalLink(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getImages().get(0)
            .getExternalLink());
    Assert.assertEquals(products.get(0).getImages().get(0).getIsCover(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getImages().get(0)
            .getIsCover());
    Assert.assertEquals(products.get(0).getImages().get(0).getEnabled(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(0).getImages().get(0)
            .getEnabled());

    Assert.assertEquals(products.get(1).getId(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getId());
    Assert.assertEquals(products.get(1).getSeller().getId(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getSeller().getId());
    Assert.assertEquals(products.get(1).getCategory().getId(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getCategory().getId());
    Assert.assertEquals(products.get(1).getSku(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getSku());
    Assert.assertEquals(products.get(1).getName(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getName());
    Assert.assertEquals(products.get(1).getDescription(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getDescription());
    Assert.assertEquals(products.get(1).getPrice(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getPrice());
    Assert.assertEquals(products.get(1).getDiscount(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getDiscount());
    Assert.assertEquals(products.get(1).getPromoCode(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getPromo().getCode());
    Assert.assertEquals(products.get(1).getPromoExpiry(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getPromo().getExpiry());
    Assert.assertEquals(products.get(1).getCheckoutLink(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getCheckoutLink());
    Assert.assertEquals(products.get(1).getEnabled(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getEnabled());
    Assert.assertEquals(products.get(1).getImages().get(0).getId(), Objects
        .requireNonNull(productsList.getBody()).getProducts().get(1).getImages().get(0).getId());
    Assert.assertEquals(products.get(1).getImages().get(0).getExternalLink(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getImages().get(0)
            .getExternalLink());
    Assert.assertEquals(products.get(1).getImages().get(0).getIsCover(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getImages().get(0)
            .getIsCover());
    Assert.assertEquals(products.get(1).getImages().get(0).getEnabled(),
        Objects.requireNonNull(productsList.getBody()).getProducts().get(1).getImages().get(0)
            .getEnabled());

    productrepository.deleteAll();
  }

  @Test
  public void createWhenSuccessfully() {
    final var content = this.getFaker().company().url();
    final var sku = this.getFaker().code().isbnRegistrant();

    Mockito.when(awsConfig.amazonS3()).thenReturn(awsComponent.amazonS3());

    final var response = productController.create(RequestBodyCreateProductDto.builder()
        .seller(IdDto.builder().id(seller.getId()).build())
        .category(IdDto.builder().id(category.getId()).build()).sku(sku)
        .name(this.getFaker().commerce().productName())
        .description(this.getFaker().commerce().productName())
        .currency(this.getFaker().finance().bic())
        .price(new BigDecimal(this.getFaker().commerce().price(1, 2).replace(",", ".")))
        .discount(this.getFaker().number().randomDigit())
        .promo(PromoDto.builder().code(this.getFaker().commerce().promotionCode())
            .expiry(ZonedDateTime.now()).build())
        .checkoutLink(this.getFaker().company().url()).enabled(this.getFaker().bool().bool())
        .images(Collections.singletonList(ImageCreateDto.builder().content(content)
            .isCover(this.getFaker().bool().bool()).enabled(this.getFaker().bool().bool()).build()))
        .build());

    final var product =
        productrepository.findById(Objects.requireNonNull(response.getBody()).getId())
            .orElseThrow(InternalError::new);

    Assert.assertEquals(product.getId(), response.getBody().getId());
    Assert.assertEquals(product.getSeller().getId(), response.getBody().getSeller().getId());
    Assert.assertEquals(product.getCategory().getId(), response.getBody().getCategory().getId());
    Assert.assertEquals(product.getSku(), response.getBody().getSku());
    Assert.assertEquals(product.getName(), response.getBody().getName());
    Assert.assertEquals(product.getDescription(), response.getBody().getDescription());
    Assert.assertEquals(product.getCurrency(), response.getBody().getCurrency());
    Assert.assertEquals(product.getPrice(), response.getBody().getPrice());
    Assert.assertEquals(product.getDiscount(), response.getBody().getDiscount());
    Assert.assertEquals(product.getPromoCode(), response.getBody().getPromo().getCode());
    Assert.assertEquals(product.getPromoExpiry(), response.getBody().getPromo().getExpiry());
    Assert.assertEquals(product.getCheckoutLink(), response.getBody().getCheckoutLink());
    Assert.assertEquals(product.getEnabled(), response.getBody().getEnabled());
    Assert.assertEquals(product.getImages().get(0).getId(),
        response.getBody().getImages().get(0).getId());
    Assert.assertEquals(product.getImages().get(0).getExternalLink(),
        response.getBody().getImages().get(0).getExternalLink());
    Assert.assertEquals(product.getImages().get(0).getEnabled(),
        response.getBody().getImages().get(0).getEnabled());
  }

  @Test
  public void createImageWhenSuccessfully() {
    final var content = this.getFaker().company().url();

    Mockito.when(awsConfig.amazonS3()).thenReturn(awsComponent.amazonS3());

    ProductDomain res = productrepository
        .save(ProductFactory.productByEnabled(Boolean.FALSE, DATE_DEFAULT, seller, category));

    final var response = productController.createImage(res.getId(),
        Collections.singletonList(
            ImageCreateDto.builder().content(content).isCover(this.getFaker().bool().bool())
                .enabled(this.getFaker().bool().bool()).build()));

    Assert.assertNotNull(response);
    Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    Assert.assertEquals(1, Objects.requireNonNull(response.getBody()).size());
  }

  @Test
  public void createImageWhenProductNotFound() {
    final var content = this.getFaker().company().url();

    final var exception = assertThrows(NotFoundException.class,
        () -> productController.createImage(100L,
            Collections.singletonList(
                ImageCreateDto.builder().content(content).isCover(this.getFaker().bool().bool())
                    .enabled(this.getFaker().bool().bool()).build())));

    Assert.assertEquals("Product", exception.getParameters()[0]);
  }

  @Test
  public void findAllProductsWhenReservationException() {
    final var accountId = this.getFaker().crypto().md5();

    mockReservationServiceException(accountId);

    productrepository.saveAll(
        Arrays.asList(ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category),
            ProductFactory.productByEnabled(Boolean.TRUE, DATE_DEFAULT, seller, category)));

    assertThrows(InternalServerErrorException.class,
        () -> productController.findAll(Boolean.TRUE, accountId));

    productrepository.deleteAll();
  }

  private void mockReservationService(@NonNull final String accountId, @NonNull final String status,
      @NonNull final ResponseBodyReservationDto.Content.DateDefinition finish) {

    Mockito.when(mockRestTemplate(accountId))
        .thenReturn(ResponseEntity.status(HttpStatus.OK)
            .body(ResponseBodyReservationDto.builder().content(Collections.singletonList(
                ResponseBodyReservationDto.Content.builder().finish(finish).status(status).build()))
                .build()));
  }

  private void mockReservationServiceException(@NonNull final String accountId) {
    Mockito.when(mockRestTemplate(accountId)).thenThrow(RestClientException.class);
  }

  private ResponseEntity<ResponseBodyReservationDto> mockRestTemplate(
      @NonNull final String accountId) {
    return restTemplate.getForEntity(
        URI.create(UriComponentsBuilder
            .fromHttpUrl(format("{0}/{1}", host, "/selfmanagement/reservations/reservations"))
            .queryParam("user_id", accountId).queryParam("site", "BRA")
            .queryParam("channel", "MOBILE").queryParam("brand", "BRAND")
            .queryParam("language", "pt").queryParam("company", "BRAND").toUriString()),
        ResponseBodyReservationDto.class);
  }
}
