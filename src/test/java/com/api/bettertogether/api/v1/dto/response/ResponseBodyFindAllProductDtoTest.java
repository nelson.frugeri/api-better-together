package com.api.bettertogether.api.v1.dto.response;

import com.api.bettertogether.template.DtoTemplateTest;
import org.junit.Test;

public class ResponseBodyFindAllProductDtoTest
    extends DtoTemplateTest<ResponseBodyFindAllProductDto> {

  @Test
  public void validateSchema() throws Exception {
    super.validateSchema("/schema/v1/dto/response/response-body-find-all-product.json");
  }
}
