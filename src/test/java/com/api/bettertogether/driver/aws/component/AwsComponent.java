package com.api.bettertogether.driver.aws.component;

import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.junit.Rule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.testcontainers.containers.localstack.LocalStackContainer;

@Component
public class AwsComponent {

  @Rule
  public LocalStackContainer localStack = new LocalStackContainer().withServices(S3);

  @Value("${aws.s3.bucketName}")
  private String bucketName;

  public AmazonS3 amazonS3() {
    localStack.start();

    final var amazonS3 =
        AmazonS3ClientBuilder.standard().withCredentials(localStack.getDefaultCredentialsProvider())
            .withEndpointConfiguration(localStack.getEndpointConfiguration(S3)).build();

    amazonS3.createBucket(bucketName);
    return amazonS3;
  }
}
