package com.api.bettertogether.config;

import com.api.bettertogether.template.TemplateTest;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration
public class HealthConfigTest extends TemplateTest {

  @Test
  public void health() {
    Assert.assertNotNull(new HealthConfig().health());
  }
}
