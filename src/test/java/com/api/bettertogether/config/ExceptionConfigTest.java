package com.api.bettertogether.config;

import static java.text.MessageFormat.format;

import com.api.bettertogether.exception.InternalServerErrorException;
import com.api.bettertogether.template.TemplateTest;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.bind.MissingRequestHeaderException;

public class ExceptionConfigTest extends TemplateTest {

  @Autowired
  private ExceptionConfig exceptionConfig;

  @MockBean
  private MissingRequestHeaderException missingRequestHeaderException;

  @Test
  public void exceptionWhenInternalServerError() {
    final var exception = exceptionConfig.exception(new InternalServerErrorException());

    Assert.assertEquals(exception.getCode(), "ERR-0001");
    Assert.assertEquals(exception.getType(), "Internal server error.");
    Assert.assertEquals(exception.getMessage(),
        "Was encountered an error when processing your request.");
  }

  // @Test
  // public void exceptionWhenNotFound() {
  // final var entity = "Reservation";
  // final var exception = exceptionConfig.exception(new NotFoundException(entity));
  //
  // Assert.assertEquals(exception.getCode(), "ERR-0002");
  // Assert.assertEquals(exception.getType(), "Not found.");
  // Assert.assertEquals(exception.getMessage(), format("{0} not found.", entity));
  // }

  @Test
  public void exceptionWhenBadRequest() {
    final var exception = exceptionConfig.exception(missingRequestHeaderException);

    Assert.assertEquals(exception.getCode(), "ERR-0003");
    Assert.assertEquals(exception.getType(), "Bad request.");
    Assert.assertEquals(exception.getMessage(),
        format("Missing request header {0}.", missingRequestHeaderException.getHeaderName()));
  }
}
