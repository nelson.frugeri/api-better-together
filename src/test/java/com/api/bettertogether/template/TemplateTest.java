package com.api.bettertogether.template;

import com.api.bettertogether.BetterTogetherApplication;
import com.github.javafaker.Faker;
import javax.transaction.Transactional;
import lombok.Getter;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {BetterTogetherApplication.class})
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
@TestPropertySource(properties = {"spring.config.location=classpath:application-test.yml"})
public abstract class TemplateTest {

  @Getter
  private final Faker faker = new Faker();
}
