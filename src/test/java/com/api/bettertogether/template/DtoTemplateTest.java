package com.api.bettertogether.template;

import com.api.bettertogether.api.v1.dto.schema.ValidateSchemaTest;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.Serializable;

@RunWith(MockitoJUnitRunner.class)
public abstract class DtoTemplateTest<D extends Serializable> extends ValidateSchemaTest {
}
