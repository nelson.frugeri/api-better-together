package com.api.bettertogether.factory;

import com.api.bettertogether.domain.CategoryDomain;
import com.api.bettertogether.domain.ImageDomain;
import com.api.bettertogether.domain.ProductDomain;
import com.api.bettertogether.domain.SellerDomain;
import com.github.javafaker.Faker;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Collections;
import lombok.NonNull;

public class ProductFactory {

  private static final Faker FAKER = new Faker();

  public static ProductDomain productByEnabled(@NonNull final Boolean enabled,
      @NonNull final ZonedDateTime zonedDateTime, @NonNull final SellerDomain seller,
      @NonNull final CategoryDomain category) {

    var product = ProductDomain.builder().seller(seller).category(category)
        .sku(FAKER.code().isbnRegistrant()).name(FAKER.commerce().productName())
        .description(FAKER.commerce().productName()).currency(FAKER.finance().bic())
        .price(new BigDecimal(FAKER.commerce().price(1, 2).replace(",", ".")))
        .discount(FAKER.number().randomDigit()).promoCode(FAKER.commerce().promotionCode())
        .promoExpiry(zonedDateTime).checkoutLink(FAKER.company().url()).enabled(enabled).build();

    product.setImages(Collections
        .singletonList(ImageDomain.builder().product(product).externalLink(FAKER.company().url())
            .isCover(FAKER.bool().bool()).enabled(enabled).build()));

    return product;
  }
}
